package com.trycatched.mindtest.tracking;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.jms.support.JmsMessageHeaderAccessor;
import org.springframework.test.context.junit4.SpringRunner;

import com.trycatched.mindtest.tracking.jms.ProcessorMessage;
import com.trycatched.mindtest.tracking.repository.TrackRepository;
import com.trycatched.mindtest.tracking.repository.TrackStepRepository;

import util.dto.ApiException;

@RunWith(SpringRunner.class)
public class ProcessorMessageTest {

	private String message;

	@InjectMocks
	private ProcessorMessage process;

	@MockBean
	private TrackRepository trackRepository;

	@MockBean
	private TrackStepRepository trackStepRepository;

	@Before
	public void setUp() throws IOException {
		MockitoAnnotations.initMocks(this);
		process = new ProcessorMessage(trackRepository, trackStepRepository);
		message = new String(Files.readAllBytes(Paths.get("src/test/resources/message.json")));
	}

	@Test
	public void saveTracking() throws ApiException {
		JmsMessageHeaderAccessor mockJmsMessageHeaderAccessor = Mockito.mock(JmsMessageHeaderAccessor.class);
		process.process(message, mockJmsMessageHeaderAccessor);
	}

}
