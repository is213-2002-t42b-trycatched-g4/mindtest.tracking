package com.trycatched.mindtest.tracking.ml;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import util.constant.ApiError;
import util.dto.ApiException;

@Component
public class MLEmotionService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public String analyzeEmotionContent(String urlEmotion, String text) throws ApiException {

		String contentResponse = null;

		try {
			URL url = new URL(urlEmotion);
			HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
			httpConnection.setRequestMethod("POST");
			httpConnection.setRequestProperty("Content-Type","application/json; utf-8");
			httpConnection.setRequestProperty("Accept", "application/json");
			httpConnection.setDoOutput(true);

			String jsonInputString = "{\"text\": \"" + text + "\", \"name\" : \"usuario\"}";
			try(OutputStream os = httpConnection.getOutputStream()) {
				byte[] input = jsonInputString.getBytes(StandardCharsets.UTF_8);
				os.write(input, 0, input.length);
			}
			try(BufferedReader br = new BufferedReader(
					new InputStreamReader(httpConnection.getInputStream(), StandardCharsets.UTF_8))) {
				StringBuilder response = new StringBuilder();
				String responseLine = null;
				while ((responseLine = br.readLine()) != null) {
					response.append(responseLine.trim());
				}
				contentResponse = response.toString();
				logger.info("Response: {}", contentResponse);
			}

		} catch (IOException e) {
			throw ApiException.of(ApiError.NO_APPLICATION_PROCESSED.getCode(), ApiError.NO_APPLICATION_PROCESSED.getMessage(), e.getMessage());
		}
		return contentResponse;
	}


}
