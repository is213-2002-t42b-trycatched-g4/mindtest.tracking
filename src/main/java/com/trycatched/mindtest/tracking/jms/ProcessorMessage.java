package com.trycatched.mindtest.tracking.jms;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.support.JmsMessageHeaderAccessor;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.trycatched.mindtest.tracking.entity.Track;
import com.trycatched.mindtest.tracking.entity.TrackStep;
import com.trycatched.mindtest.tracking.repository.TrackRepository;
import com.trycatched.mindtest.tracking.repository.TrackStepRepository;

import util.constant.ApiError;
import util.dto.ApiException;

@Component
public class ProcessorMessage {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private TrackRepository trackRepository;

	private TrackStepRepository trackStepRepository;

	@Autowired
	public ProcessorMessage(TrackRepository trackRepository, TrackStepRepository trackStepRepository) {
		this.trackRepository = trackRepository;
		this.trackStepRepository = trackStepRepository;
	}

	@JmsListener(destination = "${jms.cola.recepcion}")
	public void process(String message, JmsMessageHeaderAccessor header) throws ApiException {

		logger.info("message: {}", message);
		logger.info("UUID: {}", header.getCorrelationId());

		JsonNode rootNode = null;
		try {
			rootNode = new ObjectMapper().readTree(message);
			long idUser = rootNode.path("idUser").asLong();
			logger.info("idUser: {}", idUser);

			int idPlan = rootNode.path("idPlan").asInt();
			logger.info("idPlan: {}", idPlan);

			String planName = rootNode.path("planName").asText();
			logger.info("planName: {}", planName);

			int planActive = rootNode.path("planActive").asInt();
			logger.info("planActive: {}", planActive);

			String planDesc = rootNode.path("planDesc").asText();
			logger.info("planDesc: {}", planDesc);

			int planOrder = rootNode.path("planOrder").asInt();
			logger.info("planOrder: {}", planOrder);

			long planCreated = rootNode.path("planCreated").asLong();
			logger.info("planCreated: {}", planCreated);

			Track track = new Track();
			track.setUserId((int)idUser);
			track.setPlanId(idPlan);
			track.setUserPlanName(planName);
			track.setUserPlanIsActive(planActive);
			track.setUserPlanDescription(planDesc);
			track.setUserPlanCreatedAt(new Date(planCreated));

			logger.info("Guardando Tracking ...");
			Track trackResponse = trackRepository.save(track);
			logger.info("Tracking guardado ...");


			List<TrackStep> tracksSteps = new ArrayList<>();
			ArrayNode steps = (ArrayNode) rootNode.path("steps");
			for (JsonNode jn : steps) {

				int idStep = jn.path("idStep").asInt();
				logger.info("idStep: {}", idStep);

				String stepDesc = jn.path("stepDesc").asText();
				logger.info("stepDesc: {}", stepDesc);

				int idSstepActivetep = jn.path("stepActive").asInt();
				logger.info("idSstepActivetep: {}", idSstepActivetep);

				int stepOrder = jn.path("stepOrder").asInt();
				logger.info("stepOrder: {}", stepOrder);

				long stepCreated = jn.path("stepCreated").asLong();
				logger.info("stepCreated: {}", stepCreated);

				TrackStep trackStep = new TrackStep();
				trackStep.setTrack(trackResponse);
				trackStep.setStepDescription(stepDesc);
				trackStep.setStepActive(idSstepActivetep);
				trackStep.setStepOrder(stepOrder);
				trackStep.setUserPlanStepCreatedAt(new Date(stepCreated));

				tracksSteps.add(trackStep);
			}

			logger.info("Guardando Tracking Steps ...");
			trackStepRepository.saveAll(tracksSteps);
			logger.info("Tracking Steps guardados ...");

		} catch (IOException e) {
			throw ApiException.of(ApiError.NO_APPLICATION_PROCESSED.getCode(), ApiError.NO_APPLICATION_PROCESSED.getMessage(), e.getMessage());
		}

	}
}
