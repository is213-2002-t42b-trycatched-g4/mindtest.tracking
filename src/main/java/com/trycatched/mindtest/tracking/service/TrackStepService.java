package com.trycatched.mindtest.tracking.service;

import util.dto.ApiException;
import util.dto.ApiResponse;

public interface TrackStepService {

	public ApiResponse searchByPlanId(String authorization, int planId) throws ApiException;

}
