package com.trycatched.mindtest.tracking.service;

import util.dto.ApiException;
import util.dto.ApiResponse;

public interface TrackService {

	public ApiResponse searchByUserId(String authorization, int userId) throws ApiException;

}
