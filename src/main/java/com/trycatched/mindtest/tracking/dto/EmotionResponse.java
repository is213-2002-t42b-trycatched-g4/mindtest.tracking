package com.trycatched.mindtest.tracking.dto;

import java.io.Serializable;

public class EmotionResponse implements Serializable {

	private static final long serialVersionUID = 4248756637913715905L;
	private double sentiment;
	private EmotionScore emotionScore;
	private String tag;

	public double getSentiment() {
		return sentiment;
	}

	public void setSentiment(double sentiment) {
		this.sentiment = sentiment;
	}

	public EmotionScore getEmotionScore() {
		return emotionScore;
	}

	public void setEmotionScore(EmotionScore emotionScore) {
		this.emotionScore = emotionScore;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

}
