package com.trycatched.mindtest.tracking.dto;

import java.io.Serializable;

public class EmotionScore implements Serializable {

	private static final long serialVersionUID = 4940613682760627870L;
	private double joy;
	private double angry;
	private double fear;
	private double sad;

	public double getJoy() {
		return joy;
	}

	public void setJoy(double joy) {
		this.joy = joy;
	}

	public double getAngry() {
		return angry;
	}

	public void setAngry(double angry) {
		this.angry = angry;
	}

	public double getFear() {
		return fear;
	}

	public void setFear(double fear) {
		this.fear = fear;
	}

	public double getSad() {
		return sad;
	}

	public void setSad(double sad) {
		this.sad = sad;
	}

}
