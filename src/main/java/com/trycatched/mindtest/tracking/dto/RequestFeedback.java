package com.trycatched.mindtest.tracking.dto;

import java.io.Serializable;

public class RequestFeedback implements Serializable {

	private static final long serialVersionUID = 5850022090439516945L;

	private int stepId;
	private String responseActivity;

	public int getStepId() {
		return stepId;
	}

	public void setStepId(int stepId) {
		this.stepId = stepId;
	}

	public String getResponseActivity() {
		return responseActivity;
	}

	public void setResponseActivity(String responseActivity) {
		this.responseActivity = responseActivity;
	}

}
