package com.trycatched.mindtest.tracking.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.trycatched.mindtest.tracking.entity.TrackStep;

@Repository
public interface TrackStepRepository extends JpaRepository<TrackStep, Integer> {

	@Query(value = "select * from crm_user_plans_steps t where t.user_plan_id = :planId", nativeQuery = true)
	List<TrackStep> searchByPlanId(int planId);

}
