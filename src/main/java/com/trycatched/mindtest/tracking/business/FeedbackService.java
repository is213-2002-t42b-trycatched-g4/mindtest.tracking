package com.trycatched.mindtest.tracking.business;

import com.trycatched.mindtest.tracking.dto.RequestFeedback;

import util.dto.ApiException;
import util.dto.ApiResponse;

public interface FeedbackService {

	public ApiResponse addFeedback(String authorization, RequestFeedback requestFeedback) throws ApiException;

}
