package com.trycatched.mindtest.tracking.business;

import java.io.IOException;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trycatched.mindtest.tracking.client.ApiClientAuthorization;
import com.trycatched.mindtest.tracking.config.ApiRestConfig;
import com.trycatched.mindtest.tracking.config.MLEmotionConfig;
import com.trycatched.mindtest.tracking.dto.EmotionResponse;
import com.trycatched.mindtest.tracking.dto.EmotionScore;
import com.trycatched.mindtest.tracking.dto.RequestFeedback;
import com.trycatched.mindtest.tracking.entity.TrackStep;
import com.trycatched.mindtest.tracking.ml.MLEmotionService;
import com.trycatched.mindtest.tracking.repository.TrackStepRepository;

import util.constant.ApiError;
import util.dto.ApiException;
import util.dto.ApiResponse;

@Service
public class DefaultFeedbackService implements FeedbackService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private TrackStepRepository trackStepRepository;

	@Autowired
	private MLEmotionService mLEmotionService;

	@Autowired
	private ApiRestConfig restConfig;

	@Autowired
	private MLEmotionConfig mlConfig;

	@Autowired
	private ApiClientAuthorization apiClientAuth;

	private static final String BEARER = "Bearer ";

	@Override
	public ApiResponse addFeedback(String authorization, RequestFeedback requestFeedback) throws ApiException {
		ApiResponse authResponse = consultAuthorization(authorization);

		if (isUnauthorized(authResponse)) {
			throw ApiException.of(ApiError.USER_UNAUTHORIZAED.getCode(), ApiError.USER_UNAUTHORIZAED.getMessage());
		}

		int stepId = requestFeedback.getStepId();
		String responseActivity = requestFeedback.getResponseActivity();

		String contentEmotion = mLEmotionService.analyzeEmotionContent(mlConfig.getUrlEmotion(), responseActivity);

		TrackStep trackStep = trackStepRepository.findById(stepId)
				.orElseThrow(() -> ApiException.of(ApiError.PLAN_NOT_FOUND.getCode(), ApiError.PLAN_NOT_FOUND.getMessage()));

		JsonNode rootNode;
		double glad = 0;
		double mad = 0;
		double scared = 0;
		double sad;
		String tag;
		double sentiment;
		try {
			rootNode = new ObjectMapper().readTree(contentEmotion);
			glad = rootNode.path("emotions").path("scores").path("glad").asDouble();
			logger.info("glad: {}", glad);

			mad = rootNode.path("emotions").path("scores").path("mad").asDouble();
			logger.info("mad: {}", mad);

			scared = rootNode.path("emotions").path("scores").path("scared").asDouble();
			logger.info("scared: {}", scared);

			sad = rootNode.path("emotions").path("scores").path("sad").asDouble();
			logger.info("sad: {}", sad);

			tag = rootNode.path("emotions").path("tag").asText();
			logger.info("tag: {}", tag);

			sentiment = rootNode.path("sentiment").asDouble();
			logger.info("sentiment: {}", sentiment);

		} catch (JsonProcessingException e) {
			throw ApiException.of(ApiError.NO_APPLICATION_PROCESSED.getCode(), ApiError.NO_APPLICATION_PROCESSED.getMessage());
		}

		trackStep.setUserPlanStepFear(scared);
		trackStep.setUserPlanStepSad(sad);
		trackStep.setUserPlanStepAngry(mad);
		trackStep.setUserPlanStepJoy(glad);
		trackStep.setUserPlanStepComment(responseActivity);

		logger.info("Actualizando registros de sentimientos ...");
		trackStepRepository.save(trackStep);
		logger.info("Registro de sentimientos actualizado ...");

		EmotionScore scores = new EmotionScore();
		scores.setSad(sad);
		scores.setFear(scared);
		scores.setJoy(glad);
		scores.setAngry(mad);

		EmotionResponse emotionResponse = new EmotionResponse();
		emotionResponse.setEmotionScore(scores);
		emotionResponse.setSentiment(sentiment);
		emotionResponse.setTag(tag);

		return ApiResponse.of(ApiError.SUCCESS.getCode(), ApiError.SUCCESS.getMessage(), emotionResponse);
	}

	private ApiResponse consultAuthorization(String authorization) throws ApiException {
		String token = extractToken(authorization);
		logger.info("token: {}", token);

		String urlToken = restConfig.getUrlCheckToken();
		logger.info("urlToken: {}", urlToken);

		CloseableHttpClient httpclient = HttpClients.createDefault();
		ApiResponse authResponse;
		try {
			authResponse = apiClientAuth.verifyAuthorization(httpclient, urlToken, token);
		} catch (IOException e) {
			throw ApiException.of(ApiError.NO_APPLICATION_PROCESSED.getCode(), ApiError.NO_APPLICATION_PROCESSED.getMessage(), e.getMessage());
		}
		return authResponse;
	}

	private boolean isUnauthorized(ApiResponse authResponse) throws ApiException {
		JsonNode root = null;
		String message = authResponse.getMessage();
		try {
			root = new ObjectMapper().readTree(message);
			logger.info("result capturado: {}", root);
		} catch (Exception e) {
			throw ApiException.of(ApiError.NO_APPLICATION_PROCESSED.getCode(), ApiError.NO_APPLICATION_PROCESSED.getMessage(), e.getMessage());
		}
		return !root.path("object").asBoolean();
	}

	private String extractToken(String authorization) {
		return authorization.substring(BEARER.length(), authorization.length());
	}

}
