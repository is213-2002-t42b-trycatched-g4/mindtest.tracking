package com.trycatched.mindtest.tracking.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "crm_user_plans_steps")
public class TrackStep implements Serializable {

	private static final long serialVersionUID = 889577954947091196L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_plan_step_id")
	private Integer userPlanStepId;

	@ManyToOne
	@JoinColumn(name = "user_plan_id")
	private Track track;

	@Column(name = "step_desc")
	private String stepDescription;

	@Column(name = "user_plan_step_comment")
	private String userPlanStepComment;

	@Column(name = "step_active")
	private int stepActive;

	@Column(name = "step_order")
	private int stepOrder;

	@Column(name = "user_plan_step_joy")
	private double userPlanStepJoy;

	@Column(name = "user_plan_step_angry")
	private double userPlanStepAngry;

	@Column(name = "user_plan_step_fear")
	private double userPlanStepFear;

	@Column(name = "user_plan_step_sad")
	private double userPlanStepSad;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "user_plan_step_created_at")
	private Date userPlanStepCreatedAt;

	public Track getTrack() {
		return track;
	}

	public void setTrack(Track track) {
		this.track = track;
	}

	public Integer getUserPlanStepId() {
		return userPlanStepId;
	}

	public void setUserPlanStepId(Integer userPlanStepId) {
		this.userPlanStepId = userPlanStepId;
	}

	public String getStepDescription() {
		return stepDescription;
	}

	public void setStepDescription(String stepDescription) {
		this.stepDescription = stepDescription;
	}

	public String getUserPlanStepComment() {
		return userPlanStepComment;
	}

	public void setUserPlanStepComment(String userPlanStepComment) {
		this.userPlanStepComment = userPlanStepComment;
	}

	public int getStepActive() {
		return stepActive;
	}

	public void setStepActive(int stepActive) {
		this.stepActive = stepActive;
	}

	public int getStepOrder() {
		return stepOrder;
	}

	public void setStepOrder(int stepOrder) {
		this.stepOrder = stepOrder;
	}

	public double getUserPlanStepJoy() {
		return userPlanStepJoy;
	}

	public void setUserPlanStepJoy(double userPlanStepJoy) {
		this.userPlanStepJoy = userPlanStepJoy;
	}

	public double getUserPlanStepAngry() {
		return userPlanStepAngry;
	}

	public void setUserPlanStepAngry(double userPlanStepAngry) {
		this.userPlanStepAngry = userPlanStepAngry;
	}

	public double getUserPlanStepFear() {
		return userPlanStepFear;
	}

	public void setUserPlanStepFear(double userPlanStepFear) {
		this.userPlanStepFear = userPlanStepFear;
	}

	public double getUserPlanStepSad() {
		return userPlanStepSad;
	}

	public void setUserPlanStepSad(double userPlanStepSad) {
		this.userPlanStepSad = userPlanStepSad;
	}

	public Date getUserPlanStepCreatedAt() {
		return userPlanStepCreatedAt;
	}

	public void setUserPlanStepCreatedAt(Date userPlanStepCreatedAt) {
		this.userPlanStepCreatedAt = userPlanStepCreatedAt;
	}

	@Override
	public String toString() {
		return "TrackStep [userPlanStepId=" + userPlanStepId + ", stepDescription=" + stepDescription
				+ ", userPlanStepComment=" + userPlanStepComment + ", stepActive=" + stepActive + ", stepOrder="
				+ stepOrder + ", userPlanStepJoy=" + userPlanStepJoy + ", userPlanStepAngry=" + userPlanStepAngry
				+ ", userPlanStepFear=" + userPlanStepFear + ", userPlanStepSad=" + userPlanStepSad
				+ ", userPlanStepCreatedAt=" + userPlanStepCreatedAt + ", getUserPlanStepId()=" + getUserPlanStepId()
				+ ", getStepDescription()=" + getStepDescription() + ", getUserPlanStepComment()="
				+ getUserPlanStepComment() + ", getStepActive()=" + getStepActive() + ", getStepOrder()="
				+ getStepOrder() + ", getUserPlanStepJoy()=" + getUserPlanStepJoy() + ", getUserPlanStepAngry()="
				+ getUserPlanStepAngry() + ", getUserPlanStepFear()=" + getUserPlanStepFear()
				+ ", getUserPlanStepSad()=" + getUserPlanStepSad() + ", getUserPlanStepCreatedAt()="
				+ getUserPlanStepCreatedAt() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

}
