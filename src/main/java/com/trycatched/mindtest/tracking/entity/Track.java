package com.trycatched.mindtest.tracking.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

@Entity
@Table(name = "crm_user_plans")
public class Track implements Serializable {

	private static final long serialVersionUID = 9148406524096483885L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_plan_id")
	private Integer userPlanId;

	@Column(name = "user_id")
	private int userId;

	@Column(name = "plan_id")
	private int planId;

	@Size(max = 100)
	@Column(name = "user_plan_name")
	private String userPlanName;

	@Column(name = "user_plan_active")
	private int userPlanIsActive;

	@Column(name = "user_plan_desc")
	private String userPlanDescription;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "user_plan_created_at")
	private Date userPlanCreatedAt;

	public Integer getUserPlanId() {
		return userPlanId;
	}

	public void setUserPlanId(Integer userPlanId) {
		this.userPlanId = userPlanId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getPlanId() {
		return planId;
	}

	public void setPlanId(int planId) {
		this.planId = planId;
	}

	public String getUserPlanName() {
		return userPlanName;
	}

	public void setUserPlanName(String userPlanName) {
		this.userPlanName = userPlanName;
	}

	public int getUserPlanIsActive() {
		return userPlanIsActive;
	}

	public void setUserPlanIsActive(int userPlanIsActive) {
		this.userPlanIsActive = userPlanIsActive;
	}

	public String getUserPlanDescription() {
		return userPlanDescription;
	}

	public void setUserPlanDescription(String userPlanDescription) {
		this.userPlanDescription = userPlanDescription;
	}

	public Date getUserPlanCreatedAt() {
		return userPlanCreatedAt;
	}

	public void setUserPlanCreatedAt(Date userPlanCreatedAt) {
		this.userPlanCreatedAt = userPlanCreatedAt;
	}

}
