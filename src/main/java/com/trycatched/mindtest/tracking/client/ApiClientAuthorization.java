package com.trycatched.mindtest.tracking.client;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import util.dto.ApiResponse;

@Component
public class ApiClientAuthorization {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String ENCODE = "UTF-8";

	public ApiResponse verifyAuthorization(CloseableHttpClient httpclient, String urlRequest, String token) throws IOException {
		logger.info("Iniciando consulta a módulo de seguridad ...");
		HttpGet httpget = new HttpGet(urlRequest + "/" + token);
		ApiResponse response = httpclient.execute(httpget, new StringResponseHandler());
		String status = response.getCode();
		logger.info("STATUS CODE: {}", status);
		return response;
	}


	private static class StringResponseHandler implements ResponseHandler<ApiResponse> {
		@Override
		public ApiResponse handleResponse(HttpResponse response) throws IOException {
			int status = response.getStatusLine().getStatusCode();
			HttpEntity entity = response.getEntity();
			return ApiResponse.of(String.valueOf(status), EntityUtils.toString(entity, ENCODE));
		}
	}

}
