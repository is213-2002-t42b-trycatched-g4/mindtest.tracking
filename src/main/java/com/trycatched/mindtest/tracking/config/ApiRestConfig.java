package com.trycatched.mindtest.tracking.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "rest.api.client")
public class ApiRestConfig {

	private String urlCheckToken;

	public String getUrlCheckToken() {
		return urlCheckToken;
	}

	public void setUrlCheckToken(String urlCheckToken) {
		this.urlCheckToken = urlCheckToken;
	}

}
