package com.trycatched.mindtest.tracking.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "ml.emotion.service")
public class MLEmotionConfig {

	private String urlEmotion;

	public String getUrlEmotion() {
		return urlEmotion;
	}

	public void setUrlEmotion(String urlEmotion) {
		this.urlEmotion = urlEmotion;
	}

}
