package com.trycatched.mindtest.tracking.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trycatched.mindtest.tracking.business.FeedbackService;
import com.trycatched.mindtest.tracking.dto.RequestFeedback;

import util.dto.ApiException;
import util.dto.ApiResponse;
import util.dto.ErrorResponse;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/core")
public class FeedbackRestController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private FeedbackService feedbackService;

	@Autowired
	public FeedbackRestController(FeedbackService feedbackService) {
		this.feedbackService = feedbackService;
	}

	@PostMapping("/feedback")
	public ResponseEntity<?> addFeedback(@RequestHeader("Authorization") String authorization, @RequestBody RequestFeedback requestFeedback) {
		ApiResponse response;
		try {
			response = feedbackService.addFeedback(authorization, requestFeedback);
		} catch (ApiException e) {
			logger.error(e.getMessage(), e);
			return new ResponseEntity<>(ErrorResponse.of(e.getCode(), e.getMessage(), e.getDetailMessage()), HttpStatus.PRECONDITION_FAILED);
		}
		return ResponseEntity.ok(response);
	}

}
