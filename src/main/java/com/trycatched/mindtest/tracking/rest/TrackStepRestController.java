package com.trycatched.mindtest.tracking.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trycatched.mindtest.tracking.service.TrackStepService;

import util.dto.ApiException;
import util.dto.ApiResponse;
import util.dto.ErrorResponse;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/core")
public class TrackStepRestController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private TrackStepService trackStepService;

	@Autowired
	public TrackStepRestController(TrackStepService trackService) {
		this.trackStepService = trackService;
	}

	@GetMapping("/tracking-step/{planId}")
	public ResponseEntity<?> obtainsTrackStep(@RequestHeader("Authorization") String authorization, @PathVariable int planId) {
		ApiResponse response;
		try {
			response = trackStepService.searchByPlanId(authorization, planId);
		} catch (ApiException e) {
			logger.error(e.getMessage(), e);
			return new ResponseEntity<>(ErrorResponse.of(e.getCode(), e.getMessage(), e.getDetailMessage()), HttpStatus.PRECONDITION_FAILED);
		}
		return ResponseEntity.ok(response);
	}

}
